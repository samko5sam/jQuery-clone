class ElementCollection extends Array{
    ready(cb){
        const isReady = this.some(e => {
            return e.readyState != null && e.readyState != 'loading'
        })
        
        if (isReady){
            cb()
        }else{
            this.on("DOMContentLoaded",cb)
        }
    }
    on(event,cb_or_selector,cb){
        if (typeof cb_or_selector === 'function'){
            this.forEach(e => e.addEventListener(event,cb_or_selector))
        }else{
            this.forEach(elem => {
                elem.addEventListener(event,e => {
                    if (e.target.matches(cb_or_selector)) cb(e)
                })
            })
        }
        
    }
}

function $(param){
    if (typeof param === 'string' || param instanceof String){
        return new ElementCollection(document.querySelectorAll(param))
    }else{
        return new ElementCollection(param)
    }
}